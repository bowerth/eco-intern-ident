
## #' single function combining functions below
## #' @example
## #' str(data)
## #' cou_c <- c("ARG", "BEL")
## #' formulae <- list(MSGD = "MGS.Q * EXCH.Q")
## #' transform_xts(data = eo_q_tsv_orig, cou_c = c("AUT"), formulae = list(XSGD = "XGS.Q * EXCH.Q"))
## #' transform_xts(data = eo_q_tsv_orig, cou_c = c("BEL"), formulae = list(MSGD = "MGS.Q * EXCH.Q"))%>% head()
## transform_xts <- function(data, cou_c, formulae) {
##   data_ls <-
##     lapply(cou_c,
##            extr_cou_xts,
##            data = data
##            )
##   names(data_ls) <- cou_c
##   ## str(data_ls)

##   result_ls <-
##     lapply(data_ls,
##            function(x) lapply(formulae, apply_formula_xts, data = x))

##   result_xts <- ls2xts(result_ls)
##   ## return(result_ls)
##   return(result_xts)
## }


## #' extract columns starting with country code
## #' @example
## #' load(file = "../data/derived/orig_eo_q_tsv.Rda")
## #' dat_cou <- extr_cou_xts("AUT", eo_q_tsv_orig)
## extr_cou_xts <- function(cou, data) {
##   data_cou <-
##     data %>%
##     ## select(starts_with(cou))
##     select(Date, starts_with(cou)) # %>%
##     ## to_xts()
##   names(data_cou) <- sub(paste0(cou, "."), "", names(data_cou))
##   return(data_cou)
## }
## ## head(names(eo_q_tsv_orig))
## ## extr_cou("AUT", eo_q_tsv_orig)[, 1:5] %>% str()


#' convert data to xts
#' @example
#' load(file = "../data/derived/orig_eo_q_tsv.Rda")
#' dat_conv <- to_xts(eo_q_tsv_orig)
to_xts <- function(data) {
  data$Date <- as.Date(data$Date)
  cols <- colnames(data)
  ## data_out <- xts(data[,-1], order.by = as.Date(data[,1],"%m/%d/%Y"))
  data_out <- xts(data[,-1],
                  order.by = as.Date(data[,1],"%m/%d/%Y"))
  ## tzone(data_out) <- "UTC"
  colnames(data_out) <- cols[2:length(cols)] # Some times colnames in ts are lost
  return(data_out)
}

#' convert xts to numeric vector
#' @example
#' dat_conv <- to_xts(eo_q_tsv_orig)
from_xts <- function(data_xts) {
  data_c <- as.numeric(data_xts[, 1])
  names(data_c) <- index(data_xts)
  return(data_c)
}


#' @param formula a text string formula
#' @example
#' formula <- "XGS.Q/myenv$annual_value(OTH.XGS.Q,mydate)*lag(EXCH.Q)/OTH.EXCH.Q"
#' formula_extract(formula)
formula_extract <- function(formula) {
  ## series from current country
  series_local <-
    stringr::str_extract_all(formula, pattern = "[0-9a-zA-Z]+[.][AaQqDdSs]")[[1]] %>% unique()
  ## series from other countries beginning with country code
  series_extra <-
    stringr::str_extract_all(formula, pattern = "[0-9a-zA-Z]+[.][a-zA-Z]+[.][AaQqDdSs]")[[1]] %>% unique()
  return(union(series_local, series_extra))
}

#' @param formula a text string formula
#' @param data an xts object with variables in columns
#' @example
#' check_missing(formula = "XGS.Q * EXCH.Q", data = dat_cou)
#' ## this throws an error
#' check_missing(formula = "XXGS.Q + XGS.Q * EXCH.Q / OTH.EXCH.Q", data = dat_cou)
#' dat_cou <- extr_cou("OTH", eo_q_tsv_orig_xts, oth = c("OTH.*.Q"))
#' dat_cou <- extr_cou("OIL", eo_q_tsv_orig_xts, oth = c("OTH.*.Q"))
#' check_missing(formula, dat_cou)
check_missing <- function(formula, data) {
  required_series <- formula_extract(formula)
  missing_series <- setdiff(required_series, names(data))
  if (length(missing_series) > 0) {
    msg <- paste("the following series are not present in the data:",
                 paste(missing_series, collapse = ", "))
    ## return(msg)
    ## stop(msg)
    ## warning(msg)
    msg
  }
}


#' function to evaluate simple text formula
#'
#' @param formula a text string formula
#' @param data an xts object with variables in columns
#' add: specific reference year, e.g. 2000; country vector (incl/excl); lag
#' dat_calc <- apply_formula_xts(formula = "XGS.Q * EXCH.Q", data_cou = dat_cou)
#' dat_calc
#' dat_calc <- apply_formula_xts(formula = "XXGS.Q + XGS.Q * EXCH.Q / OTH.EXCH.Q", data = dat_cou)
#' dat_calc %>% tail()
#' ## data <- dat_cou
apply_formula_xts <- function(formula, data_cou) {
  ## ## check if required series are present in data
  msg <- check_missing(formula, data_cou)
  if(length(msg) > 0) return(msg)
  ## attach(data)
  ## result <- eval(parse(text = formula))
  ## names(result) <- Date
  ## detach(data)
  ## data_xts <- ifelse(any(class(data)=="xts"), data, to_xts(data))
  ## data_xts <- to_xts(data)
  ## data_xts <- data
  ## result_xts <- with(data, eval(parse(text = formula)))
  result_xts <- with(data_cou, eval(parse(text = formula)))
  ## data_cou <<- cbind(data_cou, result_xts)
  ## result_c <- as.numeric(result_xts[, 1])
  ## names(result_c) <- index(result_xts)
  ## if (keep) data_xts <- cbind(data_xts, result_xts)
  result_c <- from_xts(result_xts)
  ## result_ls <- list(
  ##   ## data = result_xts,
  ##   data = from_xts(result_xts),
  ##   msg = msg
  ## )
  return(result_c)
  ## return(result_xts)
  ## return(result_ls)
}
## dat <- extr_cou("AUT", eo_q_tsv_orig)
## apply_formula("XGS.Q * EXCH.Q", data = dat)




## #' @param list a list with countries on the first level and indicators on the
## #'   second level
## ls2xts <- function(list) {
##   res_xts <- lapply(list,
##                     function(x) {
##                       mat <- do.call("cbind", x)
##                       xts <- as.xts(mat)
##                     })
##   for (n in names(res_xts)) {
##     names(res_xts[[n]]) <- paste0(n, ".", names(res_xts[[n]]))
##   }
##   out <- do.call("cbind", res_xts)
##   return(out)
## }


#' function to replace existing series
#'
#' @param x xts object with original series
#' @param y xts object with new series
#' @example
#' x <- eo_q_tsv_orig[, c("ARG.XGS.Q", "ARG.MGS.Q", "BEL.XGS.Q", "BEL.MGS.Q")]
#' y <- eo_q_tsv_orig[, c("ARG.MGSVD.Q", "BEL.MGSVD.Q")]["2010/2020"]
#' names(y) <- sub("MGSVD", "MGS", names(y))
ser_replace <- function(x, y) {
  duplicate <- intersect(names(x), names(y))
  x_select <- x[, setdiff(names(x), duplicate)]
  ## tail(x_select)
  x_copy <- cbind(x_select, y)
  x_ordered <- x_copy[, names(x)]
  ## names(x_ordered)
  return(x_ordered)
}
