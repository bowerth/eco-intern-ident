MAKE=make
READ=readMergeData
WORK=work
REPORT=report

outputs=$(READ) $(WORK) $(REPORT)

.PHONY: all $(outputs)
all: $(outputs)

$(outputs):
	$(MAKE) --directory $@

include ~/lib/r-rules.mk
