setwd("c:/Users/werth_b/Downloads/r_ident")

## install.packages("xts")
library(xts)

source("\\\\asap1\\fame\\eco\\fe\\R\\Eco chart generator\\EcoChartGenerator_libraries.R")


Transf_Ts <- function(d) {
  d$Date <- as.Date(d$Date)
  cols <- colnames(d)
  ts  <- xts(d[,-1], order.by=as.Date(d[,1],"%m/%d/%Y"))
  colnames(ts) <- cols[2:length(cols)] # Some times colnames in ts are lost
  ts  }

lagQ <- function(ts) {
  lag.xts(xts(x = c(coredata(ts), NA), order.by = seq(index(ts)[1], length = length(ts) + 1, by = "quarters")), lag = 1)
}

tradebyear = as.Date("2010-10-01")

## eo_data_A <- Eco_GetDataFromPrognoz("EO_LIVE","*.PFOR2.A+*.PGDP.A+*.PMGS.A+*.PTDD.A+*.PXC.A+*.PXGS.A+*.XGSVD.A+*.XMKT.A+*.XPERF.A+*.RPMGS.A+*.RPXGS.A","1960","2020","A")
## eo_data_Q <- Eco_GetDataFromPrognoz("EO_LIVE","*.EXCH.Q+*.EXCHIN.Q+*.EXCHUD.Q+*.MAR.Q+*.MEN.Q+*.MFD.Q+*.MGS.Q+*.MGS_ADBQ.Q+*.MGSD.Q+*.MGSV.Q+*.MGSV_ADBQ.Q+*.MGSVD.Q+*.MGSVD_ADBQ.Q+*.MTB.Q+*.PGDP.Q+*.PMGS.Q+*.PMGS_ADBQ.Q+*.PMGSX.Q+*.PMNW.Q+*.PMSHX.Q+*.PTDD.Q+*.PXC.Q+*.PXGS.Q+*.PXGS_ADBQ.Q+*.PXGSX.Q+*.PXNW.Q+*.TEVD.Q+*.TGSVD.Q+*.ULCDR.Q+*.WMN.Q+*.WPBRENT.Q+*.WPHAMD.Q+*.WPHFD.Q+*.WPHMMD.Q+*.WPHTBD.Q+*.WPOIL.Q+*.WPOIL_ADBQ.Q+*.WXN.Q+*.XAR.Q+*.XEN.Q+*.XFD.Q+*.XGS.Q+*.XGS_ADBQ.Q+*.RPXGS.Q+*.XPERF.Q+*.XGSD.Q+*.XGSV.Q+*.XGSV_ADBQ.Q+*.XGSVD.Q+*.XMKT.Q+*.XTB.Q+*.XVRAS.Q+*.RPMGS.Q","1960","2020","Q")
## adb_data_Q <- Eco_GetDataFromPrognoz("ADB","*.EXFORE.Q+*.PFOR2.Q+*.PMNW.Q+*.PMSH.Q+*.PMSHX.Q+*.PXC.Q+*.PXCX.Q+*.PXNW.Q+*.XMKT.Q","1960","2020","Q")

eo_data_A <- read.delim(file = "data/eo_data_A.txt", sep = "\t", header = T)
eo_data_Q <- read.delim(file = "data/eo_data_Q.txt", sep = "\t", header = T)
adb_data_Q <- read.delim(file = "data/adb_data_Q.txt", sep = "\t", header = T)

Wld_Countries <- c("AUS"," AUT"," BEL","CAN","CHL","CZE","DNK","EST","FIN","FRA","DEU","GRC","HUN","ISL","IRL","ISR,ITA","JPN","KOR","LUX","MEX","NLD","NZL","NOR","POL","PRT","SVK","SVN","ESP","SWE","CHE","TUR","GBR","USA","ARG","BRA","CHN","IND","IDN","RUS","SAU","ZAF","COL","LVA","CRI","LTU")
Noecd_zones   <- c("DAE","OIL","ROW")
Wld           <- c("AUS","AUT","BEL","CAN","CHL","CZE","DNK","EST","FIN","FRA","DEU","GRC","HUN","ISL","IRL","ISR","ITA","JPN","KOR","LUX","MEX","NLD","NZL","NOR","POL","PRT","SVK","SVN","ESP","SWE","CHE","TUR","GBR","USA","ARG","BRA","CHN","IND","IDN","RUS","SAU","ZAF","COL","LVA","CRI","LTU","DAE","OIL","ROW")
Oecd          <- c("AUS","AUT","BEL","CAN","CHL","CZE","DNK","EST","FIN","FRA","DEU","GRC","HUN","ISL","IRL","ISR,ITA","JPN","KOR","LUX","MEX","NLD","NZL","NOR","POL","PRT","SVK","SVN","ESP","SWE","CHE","TUR","GBR","USA","LVA")
Oecd2 = c("AUS","AUT","BEL","CAN","CZE","DNK","FIN","FRA","DEU","GRC","HUN","ISL","IRL","ITA","JPN","KOR","LUX","MEX","NLD","NZL","NOR","POL","PRT","SVK","ESP","SWE","CHE","TUR","GBR","USA")
aggregates    = c("eur","G7m","litl","noecd","noram","oecd","pac","OECD_EXCL_USJPEA","NOECD_EXCL_CHN","OIA","OOP","RWD","LITL")
amers = c("USA", "CAN", "MEX", "CHL")




eo_data_A <- Transf_Ts(eo_data_A)
eo_data_Q <- Transf_Ts(eo_data_Q)
adb_data_Q <- Transf_Ts(adb_data_Q)
eo_data_A_OUT <- eo_data_A
eo_data_Q_OUT <- eo_data_Q


ea16_exchin <- eo_data_Q$EA16.EXCH.Q/apply.yearly(eo_data_Q$EA16.EXCH.Q,mean)['2005-10-01'][[1]]
## ea16_lexchin <- log(ea15_exchin)
ea16_lexchin <- log(ea16_exchin)
wpoil <- eo_data_Q$OTH.WPOIL.Q


wpbrent = eo_data_Q$OTH.WPBRENT.Q

for (cou in Wld){
  eo_data_Q_OUT[,paste0(cou,".WPOIL.","Q")]<-wpoil
  pxnw=adb_data_Q[,paste0(cou,".PXNW.","Q")]
  pmnw=adb_data_Q[,paste0(cou,".PMNW.","Q")] 
  if (cou != "NZL"){
    pxnw_m<-merge(pxnw,eo_data_Q)[,1]
    for (i in (which(index(pxnw_m)==index(pxnw[length(pxnw),]))+1):length(index(pxnw_m))) {
      pxnw_m[i]<-lagQ(pxnw_m)[i]*(lagQ(eo_data_Q[,paste0(cou,".XFD.","Q")])[i]*eo_data_Q[,paste0("OTH.WPHFD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHFD.","Q")])[i]+lagQ(eo_data_Q[,paste0(cou,".XTB.","Q")])[i]*eo_data_Q[,paste0("OTH.WPHTBD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHTBD.","Q")])[i]+lagQ(eo_data_Q[,paste0(cou,".XEN.","Q")])[i]*wpoil[i]/lagQ(wpoil)[i]+lagQ(eo_data_Q[,paste0(cou,".XAR.","Q")])[i]*eo_data_Q[,paste0("OTH.WPHAMD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHAMD.","Q")])[i]+(1-lagQ(eo_data_Q[,paste0(cou,".XFD.","Q")])[i] -lagQ(eo_data_Q[,paste0(cou,".XTB.","Q")])[i]-lagQ(eo_data_Q[,paste0(cou,".XEN.","Q")])[i]-lagQ(eo_data_Q[,paste0(cou,".XAR.","Q")])[i])*eo_data_Q[,paste0("OTH.WPHMMD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHMMD.","Q")])[i])/(eo_data_Q[,paste0(cou,".EXCH.","Q")][i]/lagQ(eo_data_Q[,paste0(cou,".EXCH.","Q")])[i])
    }
    
    #   wpoil[which(index(wpoil)==index(pxnw[length(pxnw)])):which(index(wpoil)==index(wpoil[length(wpoil)]))]
  }
  pmnw_m<-merge(pmnw,eo_data_Q)[,1]
  for (i in (which(index(pmnw_m)==index(pmnw[length(pmnw),]))+1):length(index(pmnw_m))) {
    pmnw_m[i]<-lagQ(pmnw_m)[i]*(lagQ(eo_data_Q[,paste0(cou,".MFD.","Q")])[i]*eo_data_Q[,paste0("OTH.WPHFD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHFD.","Q")])[i]+lagQ(eo_data_Q[,paste0(cou,".MTB.","Q")])[i]*eo_data_Q[,paste0("OTH.WPHTBD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHTBD.","Q")])[i]+lagQ(eo_data_Q[,paste0(cou,".MEN.","Q")])[i]*wpoil[i]/lagQ(wpoil)[i]+lagQ(eo_data_Q[,paste0(cou,".MAR.","Q")])[i]*eo_data_Q[,paste0("OTH.WPHAMD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHAMD.","Q")])[i]+(1-lagQ(eo_data_Q[,paste0(cou,".MFD.","Q")])[i] -lagQ(eo_data_Q[,paste0(cou,".MTB.","Q")])[i]-lagQ(eo_data_Q[,paste0(cou,".MEN.","Q")])[i]-lagQ(eo_data_Q[,paste0(cou,".MAR.","Q")])[i])*eo_data_Q[,paste0("OTH.WPHMMD.","Q")][i]/lagQ(eo_data_Q[,paste0("OTH.WPHMMD.","Q")])[i])/(eo_data_Q[,paste0(cou,".EXCH.","Q")][i]/lagQ(eo_data_Q[,paste0(cou,".EXCH.","Q")])[i])
  }
  if (length(grep(cou, Wld_Countries))!=0){
    assign(paste0(cou, ".lpgdp"), log(eo_data_Q[,paste0(cou,".PGDP.","Q")]/apply.yearly(eo_data_Q[,paste0(cou,".PGDP.","Q")], mean)[tradebyear,][[1]]))
  }
  pxnw_m<-pxnw_m/apply.yearly(pxnw_m, mean)[tradebyear,][[1]]
  pmnw_m<-pmnw_m/apply.yearly(pmnw_m, mean)[tradebyear,][[1]]
  if (cou != "NZL"){
    eo_data_Q_OUT[,paste0(cou,".PXNW.","Q")]<-pxnw_m
  }
  eo_data_Q_OUT[,paste0(cou,".PMNW.","Q")]<-pmnw_m
  
  pxgsx=exp((log(eo_data_Q[,paste0(cou,".PXGS.","Q")]/apply.yearly(eo_data_Q[,paste0(cou,".PXGS.","Q")], mean)[tradebyear,][[1]])-eo_data_Q[,paste0(cou,".WXN.","Q")]*log(eo_data_Q_OUT[,paste0(cou,".PXNW.","Q")])) / (1-eo_data_Q[,paste0(cou,".WXN.","Q")]))
  pmgsx=exp((log(eo_data_Q[,paste0(cou,".PMGS.","Q")]/apply.yearly(eo_data_Q[,paste0(cou,".PMGS.","Q")], mean)[tradebyear,][[1]])-eo_data_Q[,paste0(cou,".WMN.","Q")]*log(eo_data_Q_OUT[,paste0(cou,".PMNW.","Q")])) / (1-eo_data_Q[,paste0(cou,".WMN.","Q")]))
  
  eo_data_Q_OUT[,paste0(cou,".PXGSX.","Q")]<-pxgsx
  eo_data_Q_OUT[,paste0(cou,".PMGSX.","Q")]<-pmgsx
  
}


seq(index(pxnw)[1], length = length(pxnw)+1, by = "quarters")
xts(x=c(coredata(pxnw),NA),order.by =seq(index(pxnw)[1], length = length(pxnw)+1, by = "quarters"))
a<-xts(c(1,2,3,4),seq(as.Date("2010-01-01"), length = 4, by = "quarters"))
#moving avearage 
rollapply(eo_data_Q$FRA.EXCH.Q  , 4, mean)


a<-xts(c(1,2,3,4,5,6,7,8),seq(as.Date("2010-01-01"), length = 8, by = "quarters"))
b<-xts(c(2,3,4,5,6,7,8,9),seq(as.Date("2010-01-01"), length = 8, by = "quarters"))

for (i in 6:length(a)) {
  a[i] <- a[i-1]+b[i-1]
}

for (i in 6:length(a)) {
  a[i] <- a[i-1]*2
}


b<-xts(c(1,2,3,4,5,6),seq(as.Date("2010-01-01"), length = 6, by = "quarters"))
a + merge(b, index(a),fill = na.locf)
merge(b, index(a),fill = na.spline)