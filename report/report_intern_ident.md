.---
title: "Intern Idents"
author: ""
date: "2018-08-08"
output:
#  pdf_document
  html_document
documentclass: article
classoption: a4paper
---








```r
## Input Data
eo_q_tsv_xts[1:10, c("AUT.MGS.Q", "AUT.EXCH.Q", "BEL.MGS.Q", "BEL.EXCH.Q")]
```

```
##            AUT.MGS.Q AUT.EXCH.Q  BEL.MGS.Q BEL.EXCH.Q
## 1960-01-01        NA  0.5287894 4808340984  0.8086266
## 1960-04-01        NA  0.5285748 4949474140  0.8090699
## 1960-07-01        NA  0.5304583 5076950400  0.8070638
## 1960-10-01        NA  0.5290822 5194849330  0.8111256
## 1961-01-01        NA  0.5277389 5332966568  0.8087065
## 1961-04-01        NA  0.5273951 5444304085  0.8066098
## 1961-07-01        NA  0.5310561 5560430267  0.8100924
## 1961-10-01        NA  0.5319683 5687551977  0.8102404
## 1962-01-01        NA  0.5318312 5735218651  0.8102676
## 1962-04-01        NA  0.5324685 5901533809  0.8103880
```

```r
## Calculate Indicators

result_xts <- transform(data = eo_q_tsv_orig,
                        cou_c = c("AUT", "BEL"),
                        formulae = list(
                          XGSD.Q = "XGS.Q * EXCH.Q",
                          MGSD.Q = "MGS.Q * EXCH.Q" #,
                          ## test = "myenv$pct(MGS.Q)"
                        ),
                        xts = TRUE)

result_xts[1:10, ]
```

```
##            AUT.XGSD.Q AUT.MGSD.Q BEL.XGSD.Q BEL.MGSD.Q
## 1960-01-01         NA         NA 3816046140 3888152298
## 1960-04-01         NA         NA 3898928547 4004470390
## 1960-07-01         NA         NA 3980674192 4097422847
## 1960-10-01         NA         NA 4102880732 4213675412
## 1961-01-01         NA         NA 4150861285 4312804981
## 1961-04-01         NA         NA 4259771581 4391428980
## 1961-07-01         NA         NA 4402743250 4504462165
## 1961-10-01         NA         NA 4527800404 4608284625
## 1962-01-01         NA         NA 4657423787 4647061655
## 1962-04-01         NA         NA 4770239508 4782532292
```


![](img/plot-calc-1.png)<!-- -->


### Comparison with published data




### Calculate new series using local function


```r
myenv <- new.env()
myenv$pct <- function(s) {
  s / lag(s)
}

result_xts_2 <- transform(data = eo_q_tsv_orig,
                        cou_c = c("AUT", "BEL"),
                        formulae = list(
                          MGSPCT.Q = "myenv$pct(MGS.Q)",
                          XGSPCT.Q = "myenv$pct(XGS.Q)"
                        ),
                        xts = TRUE)

result_xts_2[1:10, ]
```

```
##            AUT.MGSPCT.Q AUT.XGSPCT.Q BEL.MGSPCT.Q BEL.XGSPCT.Q
## 1960-01-01           NA           NA           NA           NA
## 1960-04-01           NA           NA     1.029352     1.021160
## 1960-07-01           NA           NA     1.025756     1.023504
## 1960-10-01           NA           NA     1.023222     1.025539
## 1961-01-01           NA           NA     1.026587     1.014721
## 1961-04-01           NA           NA     1.020877     1.028906
## 1961-07-01           NA           NA     1.021330     1.029120
## 1961-10-01           NA           NA     1.022862     1.028216
## 1962-01-01           NA           NA     1.008381     1.028594
## 1962-04-01           NA           NA     1.028999     1.024071
```


![](img/plot-calc-env-1.png)<!-- -->


### Custom function using xts



```r
myenv$annual_value <- function(myseries,year) {
  apply.yearly(myseries, mean)[year][[1]]
}

## source("../lib/transform_xts.R")

mydate<-as.Date("2015-10-01")
result_xts_3 <- transform(data = eo_q_tsv_orig, cou_c = c("ARG", "BEL"),
                  formulae = list(toto.A = "XGS.Q/myenv$annual_value(XGS.Q,mydate)"),
                  xts = TRUE)

result_xts_3[1:10, ]
```

```
##            ARG.toto.A BEL.toto.A
## 1960-01-01         NA 0.01423619
## 1960-04-01         NA 0.01453743
## 1960-07-01         NA 0.01487911
## 1960-10-01         NA 0.01525910
## 1961-01-01         NA 0.01548373
## 1961-04-01         NA 0.01593130
## 1961-07-01         NA 0.01639521
## 1961-10-01         NA 0.01685783
## 1962-01-01         NA 0.01733986
## 1962-04-01         NA 0.01775724
```

```r
## tzone(result_xts_3) <- "UTC"

## tzone(result_xts_3)
```


![](img/plot-calc-env-3-1.png)<!-- -->




### Custom function using recursion



```r
index <- paste0(c(2000:2015), "-01-01")
myseries <- c(rep(NA, 3), seq(1:10), rep(NA, 3)); names(myseries) <- index; myseries <- as.xts(myseries)
myseries2 <- seq(1.00, 1.20, length.out = 16); names(myseries2) <- index; myseries2 <- as.xts(myseries2)


myenv$rec <- function(myseries,myseries2,direction="forward") {
  res <- myseries
  if(direction %in% c("forward", "both")) {
    range_f <- c(max(which(!is.na((myseries))))+1, length(myseries2))
    for (i in c(range_f[1]:range_f[2])) {
      res[i][[1]] <- res[i - 1][[1]]*myseries2[i][[1]]
    }
  }
  if(direction %in% c("backward", "both")) {
    range_b <- c(1, min(which(!is.na((myseries)))) - 1)
    for (i in c(range_b[2]:range_b[1])) {
      res[i][[1]] <- res[i+1][[1]] * myseries2[i][[1]]
    }
  }
  names(res) <- names(myseries)
  return(res)
}

res <- myenv$rec(myseries, myseries2)
cbind(myseries, myseries2, res)
```

```
##            ..1      ..2      ..3
## 2000-01-01  NA 1.000000       NA
## 2001-01-01  NA 1.013333       NA
## 2002-01-01  NA 1.026667       NA
## 2003-01-01   1 1.040000  1.00000
## 2004-01-01   2 1.053333  2.00000
## 2005-01-01   3 1.066667  3.00000
## 2006-01-01   4 1.080000  4.00000
## 2007-01-01   5 1.093333  5.00000
## 2008-01-01   6 1.106667  6.00000
## 2009-01-01   7 1.120000  7.00000
## 2010-01-01   8 1.133333  8.00000
## 2011-01-01   9 1.146667  9.00000
## 2012-01-01  10 1.160000 10.00000
## 2013-01-01  NA 1.173333 11.73333
## 2014-01-01  NA 1.186667 13.92356
## 2015-01-01  NA 1.200000 16.70827
```

```r
res <- myenv$rec(myseries, myseries2, "both")
cbind(myseries, myseries2, res)
```

```
##            ..1      ..2       ..3
## 2000-01-01  NA 1.000000  1.040356
## 2001-01-01  NA 1.013333  1.040356
## 2002-01-01  NA 1.026667  1.026667
## 2003-01-01   1 1.040000  1.000000
## 2004-01-01   2 1.053333  2.000000
## 2005-01-01   3 1.066667  3.000000
## 2006-01-01   4 1.080000  4.000000
## 2007-01-01   5 1.093333  5.000000
## 2008-01-01   6 1.106667  6.000000
## 2009-01-01   7 1.120000  7.000000
## 2010-01-01   8 1.133333  8.000000
## 2011-01-01   9 1.146667  9.000000
## 2012-01-01  10 1.160000 10.000000
## 2013-01-01  NA 1.173333 11.733333
## 2014-01-01  NA 1.186667 13.923556
## 2015-01-01  NA 1.200000 16.708267
```


### Replace Result


```r
## names(eo_q_tsv_orig)

data_orig <- eo_q_tsv_orig[, c("ARG.XGS.Q", "ARG.MGS.Q", "BEL.XGS.Q", "BEL.MGS.Q")]

data_new <- eo_q_tsv_orig[, c("ARG.MGSVD.Q", "BEL.MGSVD.Q")]["2010/2020"]
names(data_new) <- sub("MGSVD", "MGS", names(data_new))

data_orig %>% tail(10)
```

```
##               ARG.XGS.Q    ARG.MGS.Q    BEL.XGS.Q    BEL.MGS.Q
## 2018-07-01 1.306825e+12 1.645893e+12 391959955369 387486072996
## 2018-10-01 1.337261e+12 1.692385e+12 398091733621 393675941931
## 2019-01-01 1.389861e+12 1.747534e+12 404378894021 399930331359
## 2019-04-01 1.452477e+12 1.806281e+12 410666893781 406186797293
## 2019-07-01 1.515654e+12 1.862091e+12 417052670507 412541138689
## 2019-10-01 1.571391e+12 1.914491e+12 423537744607 418994886700
## 2020-01-01           NA           NA           NA           NA
## 2020-04-01           NA           NA           NA           NA
## 2020-07-01           NA           NA           NA           NA
## 2020-10-01           NA           NA           NA           NA
```

```r
data_new %>% tail(10)
```

```
##               ARG.MGS.Q    BEL.MGS.Q
## 2018-07-01 104841131443 503001615186
## 2018-10-01 106413748415 508494307462
## 2019-01-01 108009954641 514046979005
## 2019-04-01 109630103961 519535848027
## 2019-07-01 111164925416 525083325861
## 2019-10-01 112721234372 530690038319
## 2020-01-01           NA           NA
## 2020-04-01           NA           NA
## 2020-07-01           NA           NA
## 2020-10-01           NA           NA
```

```r
ser_replace(x = data_orig, y = data_new) %>% tail(20)
```

```
##               ARG.XGS.Q    ARG.MGS.Q    BEL.XGS.Q    BEL.MGS.Q
## 2016-01-01 1.003162e+12  80494418278 331364000000 434096149164
## 2016-04-01 9.820877e+11  80757496383 349940000000 460696565084
## 2016-07-01 1.053156e+12  79281509118 354504000000 474777596915
## 2016-10-01 1.083428e+12  81837175051 366428000000 481218083749
## 2017-01-01 1.138183e+12  85867398594 371380000000 478734120560
## 2017-04-01 1.123447e+12  90030537009 371596000000 482454824914
## 2017-07-01 1.213376e+12  94115069317 366792000000 478891333420
## 2017-10-01 1.246232e+12  99769107090 375772000000 486411348559
## 2018-01-01 1.275315e+12 101465181911 380563656245 491958118344
## 2018-04-01 1.289660e+12 103190090003 385830212239 497449221660
## 2018-07-01 1.306825e+12 104841131443 391959955369 503001615186
## 2018-10-01 1.337261e+12 106413748415 398091733621 508494307462
## 2019-01-01 1.389861e+12 108009954641 404378894021 514046979005
## 2019-04-01 1.452477e+12 109630103961 410666893781 519535848027
## 2019-07-01 1.515654e+12 111164925416 417052670507 525083325861
## 2019-10-01 1.571391e+12 112721234372 423537744607 530690038319
## 2020-01-01           NA           NA           NA           NA
## 2020-04-01           NA           NA           NA           NA
## 2020-07-01           NA           NA           NA           NA
## 2020-10-01           NA           NA           NA           NA
```


### Check Missing Series


```r
tryCatch({
  transform(data = eo_q_tsv_orig,
            cou_c = c("ARG", "BEL"),
            ## oth = c("OTH.WPOIL.Q", "OTH.WPHAMD.Q"),
            formulae = list(toto.A = "XGS.Q/OTH.WPOIL.Q * OTH.WPHAMD.Q"))
}, error=function(e){cat("ERROR :",conditionMessage(e), "\n")})
```

```
## NULL
```

```r
result_xts_5 <- transform(data = eo_q_tsv_orig,
          cou_c = c("ARG", "BEL"),
          oth = c("OTH.WPOIL.Q", "OTH.WPHAMD.Q"),
          formulae = list(toto.A = "XGS.Q/OTH.WPOIL.Q * OTH.WPHAMD.Q"))
```


### Select additional series using wildcard

- Use expressions such as `"OTH.*.Q"`


```r
result_xts_6 <- transform(data = eo_q_tsv_orig,
                          cou_c = c("ARG", "BEL"),
                          oth = c("OTH.*.Q"),
                          formulae = list(toto.A = "XGS.Q/OTH.WPOIL.Q * OTH.WPHAMD.Q"))
```


### Complex formula, many countries


```r
Wld <- c("AUS","AUT","BEL","CAN","CHL","CZE","DNK","EST","FIN","FRA","DEU","GRC","HUN","ISL","IRL","ISR","ITA","JPN","KOR","LUX","MEX","NLD","NZL","NOR","POL","PRT","SVK","SVN","ESP","SWE","CHE","TUR","GBR","USA","ARG","BRA","CHN","IND","IDN","RUS","SAU","ZAF","COL","LVA","CRI","LTU","DAE","OIL","ROW")

result_xts_7 <- transform(data = eo_q_tsv_orig,
                        cou_c = Wld,
                        oth=c("OTH.*.Q"),
                        formulae = list(
                          int.Q = paste0("(lag(XFD.Q)*OTH.WPHFD.Q/lag(OTH.WPHFD.Q)+",
                                         "lag(XTB.Q)*OTH.WPHTBD.Q/lag(OTH.WPHTBD.Q)+",
                                         "lag(XEN.Q)*OTH.WPOIL.Q/lag(OTH.WPOIL.Q)+",
                                         "lag(XAR.Q)*OTH.WPHAMD.Q/lag(OTH.WPHAMD.Q)+",
                                         "(1-lag(XFD.Q) -lag(XTB.Q)-lag(XEN.Q)-lag(XAR.Q))*",
                                         "OTH.WPHMMD.Q/lag(OTH.WPHMMD.Q))/(EXCH.Q/lag(EXCH.Q))")
                        ))
```


### Combine EO with ADB series



```r
adb_series_add <- find_series(x = c("*.PMNW.Q"), series = names(adb_q_tsv_orig))
adb_select <- adb_q_tsv_orig[, adb_series_add]
eo_plus <- cbind_db(x = eo_q_tsv_orig, y = adb_select, code_y = "ADB")

result_xts_7 <- transform(data = eo_plus,
                          cou_c = Wld,
                          oth=c("OTH.*.Q"),
                          formulae = list(
                            int.Q = paste0("lag(XFD.Q) * OTH.WPHFD.Q + ADB_PMNW.Q")
                          ))

result_xts_7 %>% tail(20)
```

```
##            AUS.int.Q AUT.int.Q BEL.int.Q CAN.int.Q CHL.int.Q CZE.int.Q
## 2016-01-01 0.9011912  1.106157  0.977164  1.051970  1.070809  1.036383
## 2016-04-01 1.0500994  1.285449  1.185425  1.218011  1.291614  1.241545
## 2016-07-01 1.0408529  1.270189  1.184810  1.219381  1.258328  1.238810
## 2016-10-01 1.0828995  1.328281  1.247116  1.271394  1.296314  1.303685
## 2017-01-01 1.1483211  1.422345  1.342169  1.341965  1.372688  1.401253
## 2017-04-01 1.0912652  1.312301  1.237537  1.282971  1.312438  1.271280
## 2017-07-01 1.0560024  1.267009  1.193586  1.226279  1.293008  1.205321
## 2017-10-01 1.1620868  1.308418  1.256209  1.302288  1.368086  1.242333
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            DNK.int.Q EST.int.Q FIN.int.Q FRA.int.Q DEU.int.Q GRC.int.Q
## 2016-01-01  1.307045 0.9362087 0.7664383  1.111405  1.046852 0.8792518
## 2016-04-01  1.549074 1.0954153 0.9035270  1.313390  1.246003 1.0888345
## 2016-07-01  1.511531 1.1073321 0.9220699  1.301929  1.238594 1.0930024
## 2016-10-01  1.560595 1.1699899 0.9948190  1.357602  1.299457 1.1522630
## 2017-01-01  1.660329 1.2527760 1.0884532  1.452512  1.396508 1.2458106
## 2017-04-01  1.540809 1.1467248 0.9931175  1.344544  1.289366 1.1510287
## 2017-07-01  1.488459 1.0964741 0.9525209  1.295827  1.244888 1.1046537
## 2017-10-01  1.520021 1.1521123 1.0240694  1.354594  1.302535 1.1848837
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            HUN.int.Q ISL.int.Q IRL.int.Q ISR.int.Q ITA.int.Q JPN.int.Q
## 2016-01-01  1.240525  1.350538  1.379294  1.043869  1.064227 0.8460610
## 2016-04-01  1.488296  1.572432  1.613313  1.172912  1.268624 1.0110315
## 2016-07-01  1.468565  1.493929  1.578685  1.149517  1.257910 0.9706371
## 2016-10-01  1.526455  1.475029  1.624070  1.173392  1.320203 1.0643099
## 2017-01-01  1.641252  1.551312  1.719009  1.226647  1.422623 1.1893544
## 2017-04-01  1.523635  1.431580  1.599453  1.134437  1.314283 1.0999900
## 2017-07-01  1.460514  1.446489  1.548702  1.136920  1.268976 1.1176972
## 2017-10-01  1.552522  1.492488  1.586670  1.183498  1.330128 1.2271367
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            KOR.int.Q LUX.int.Q MEX.int.Q NLD.int.Q NZL.int.Q NOR.int.Q
## 2016-01-01 0.5976790  1.270674  1.117003  1.004641  1.279212  1.043701
## 2016-04-01 0.7417342  1.437372  1.403087  1.245925  1.480284  1.211158
## 2016-07-01 0.7264272  1.423221  1.418433  1.237052  1.409310  1.210725
## 2016-10-01 0.7826764  1.484492  1.519274  1.295413  1.436360  1.256061
## 2017-01-01 0.8507803  1.579088  1.660540  1.391945  1.512551  1.345246
## 2017-04-01 0.7882362  1.457204  1.468467  1.286688  1.441739  1.271892
## 2017-07-01 0.8042189  1.418603  1.436734  1.239667  1.423704  1.220285
## 2017-10-01 0.8669451  1.448881  1.600897  1.301033  1.512786  1.287064
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            POL.int.Q PRT.int.Q SVK.int.Q SVN.int.Q ESP.int.Q SWE.int.Q
## 2016-01-01  1.243568  1.003245 0.9415403 0.9420989  1.152308 0.9405193
## 2016-04-01  1.477061  1.212007 1.1276493 1.1685434  1.378906 1.1285931
## 2016-07-01  1.453605  1.197897 1.1317196 1.1611693  1.361808 1.1455510
## 2016-10-01  1.524625  1.252313 1.1944906 1.2244186  1.420040 1.2267956
## 2017-01-01  1.620442  1.343811 1.2872381 1.3222221  1.522877 1.2941845
## 2017-04-01  1.473130  1.242250 1.1848570 1.2179222  1.412054 1.2110779
## 2017-07-01  1.432876  1.192758 1.1390388 1.1743406  1.368863 1.1502142
## 2017-10-01  1.479735  1.244441 1.2049192 1.2287268  1.427719 1.2313893
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            CHE.int.Q TUR.int.Q GBR.int.Q USA.int.Q ARG.int.Q BRA.int.Q
## 2016-01-01 0.7879620  1.550541 0.8710905 0.9129326  2.467038  1.562758
## 2016-04-01 0.9353588  1.828307 1.0657774 1.1218806  2.983514  1.740924
## 2016-07-01 0.9325522  1.838566 1.1298712 1.1081423  3.108113  1.627005
## 2016-10-01 0.9708839  2.064577 1.2081598 1.1311958  3.310788  1.694259
## 2017-01-01 1.0285935  2.447352 1.2841741 1.2033833  3.617488  1.761164
## 2017-04-01 0.9544519  2.241037 1.1815065 1.1379281  3.423837  1.695351
## 2017-07-01 0.9446638  2.252295 1.1708191 1.1474074  3.746560  1.695254
## 2017-10-01 1.0050613  2.531736 1.2136576 1.2110035  4.131600  1.855706
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            CHN.int.Q IND.int.Q IDN.int.Q RUS.int.Q SAU.int.Q ZAF.int.Q
## 2016-01-01 0.9572555 0.9838851 0.9215219  1.884336 0.7634025  1.317857
## 2016-04-01 1.1392291 1.2105726 1.1200935  1.922617 0.8566915  1.564946
## 2016-07-01 1.1299337 1.2175628 1.1108234  1.857270 0.8454342  1.490769
## 2016-10-01 1.1826528 1.2798280 1.1599977  1.837585 0.8591387  1.530590
## 2017-01-01 1.2797642 1.3926065 1.2580605  1.789589 0.8969372  1.580005
## 2017-04-01 1.2046814 1.2742339 1.1816454  1.638782 0.8409417  1.487836
## 2017-07-01 1.2091832 1.2905313 1.1962680  1.703445 0.8503709  1.502121
## 2017-10-01 1.2572185 1.4255189 1.3068891  1.696458 0.8544488  1.677495
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            COL.int.Q LVA.int.Q CRI.int.Q LTU.int.Q DAE.int.Q OIL.int.Q
## 2016-01-01  1.078798  1.055787  1.311669 0.9903083 0.7716581 0.6807335
## 2016-04-01  1.219026  1.251613  1.577839 1.1847697 0.9670223 0.8194206
## 2016-07-01  1.192363  1.247099  1.539288 1.1864379 0.9585479 0.8140356
## 2016-10-01  1.250802  1.303125  1.552691 1.2504328 0.9848921 0.8348716
## 2017-01-01  1.298033  1.387162  1.646576 1.3497227 1.0570081 0.8883725
## 2017-04-01  1.225844  1.276941  1.582169 1.2448502 0.9999061 0.8373556
## 2017-07-01  1.257853  1.225790  1.607573 1.1974539 1.0105121 0.8467272
## 2017-10-01  1.341495  1.269274  1.645348 1.2719046 1.0804933 0.8884520
## 2018-01-01        NA        NA        NA        NA        NA        NA
## 2018-04-01        NA        NA        NA        NA        NA        NA
## 2018-07-01        NA        NA        NA        NA        NA        NA
## 2018-10-01        NA        NA        NA        NA        NA        NA
## 2019-01-01        NA        NA        NA        NA        NA        NA
## 2019-04-01        NA        NA        NA        NA        NA        NA
## 2019-07-01        NA        NA        NA        NA        NA        NA
## 2019-10-01        NA        NA        NA        NA        NA        NA
## 2020-01-01        NA        NA        NA        NA        NA        NA
## 2020-04-01        NA        NA        NA        NA        NA        NA
## 2020-07-01        NA        NA        NA        NA        NA        NA
## 2020-10-01        NA        NA        NA        NA        NA        NA
##            ROW.int.Q
## 2016-01-01 0.8897993
## 2016-04-01 1.0801353
## 2016-07-01 1.0616807
## 2016-10-01 1.0841581
## 2017-01-01 1.1559016
## 2017-04-01 1.0932526
## 2017-07-01 1.1032471
## 2017-10-01 1.1592917
## 2018-01-01        NA
## 2018-04-01        NA
## 2018-07-01        NA
## 2018-10-01        NA
## 2019-01-01        NA
## 2019-04-01        NA
## 2019-07-01        NA
## 2019-10-01        NA
## 2020-01-01        NA
## 2020-04-01        NA
## 2020-07-01        NA
## 2020-10-01        NA
```


### Error reporting

The series with identified missing series are missing from the result


```r
tradebaseyear <- 2015

tryCatch({
  result_xts <- transform(data = eo_q_tsv_orig,
                          cou_c = c("AUT", "BEL"),
                          formulae = list(
                            XGSD.Q = "XXGS.Q * EXCH.Q",
                            MGSD.Q = "MGS.Q * EXCH.Q",
                            test = "myenv$pct(MGS.Q)",
                            PMGSX.Q ="exp((log(PMGS.Q/PMGS.Q,tradebaseyear)-WXN.Q*log(PMNW.Q)) / (1-WMN.Q))",
                            PXGSX.Q ="exp((log(PFFFXGS.Q/PXGS.Q,tradebaseyear)-WXN.Q*log(PXNW.Q)) / (1-WXN.Q))"
                          ),
                          xts = TRUE,
                          msg = TRUE
                          )
}, error=function(e){cat("ERROR :",conditionMessage(e), "\n")})
```

```
## ERROR : AUT.XGSD.Q
##   the following series are not present in the data: XXGS.Q
## AUT.PXGSX.Q
##   the following series are not present in the data: PFFFXGS.Q
## BEL.XGSD.Q
##   the following series are not present in the data: XXGS.Q
## BEL.PXGSX.Q
##   the following series are not present in the data: PFFFXGS.Q
```





