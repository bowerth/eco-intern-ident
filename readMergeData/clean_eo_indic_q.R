
library(stringr)
library(dplyr)

source("../lib/conv_q_date.R")

load(file = "../data/derived/orig_eo_indic_q.Rda")

head(eo_indic_q_orig)

eo_indic_q_cl <-
  eo_indic_q_orig %>%
  mutate(date = conv_q_date(TIME)) %>%
  select(LOCATION, VARIABLE, FREQUENCY, date, Value)

names(eo_indic_q_cl) <- tolower(names(eo_indic_q_cl))

save(eo_indic_q_cl, file = "../data/derived/clean_eo_indic_q.Rda")
