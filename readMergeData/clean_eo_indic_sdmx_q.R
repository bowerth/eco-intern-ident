
library(dplyr)
library(xts)
library(dygraphs)

load(file = "../data/derived/orig_eo_indic_sdmx_q.Rda")

head(eo_indic_sdmx_q_orig)

eo_indic_sdmx_q_cl <- as.data.frame(eo_indic_sdmx_q_orig)

rownames(eo_indic_sdmx_q_cl) <- eo_indic_sdmx_q_cl$TIME_PERIOD

save(eo_indic_sdmx_q_cl, file = "../data/derived/clean_eo_indic_sdmx_q.Rda")
